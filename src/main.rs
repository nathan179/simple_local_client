use image::{DynamicImage, GenericImage, Rgba};
use std::{thread, time};

static WIDTH: u32 = 24;
static HEIGHT: u32 = 24;

fn main() {

    println!("Hello, world!");

    // let img = image::open(&Path::new("clients/simple_local_client/assets/favicon.png")).unwrap();

    let true_colour = true;
    // let (width, height) = determine_size(args, orig_width, orig_height);
    let (width, height) = (WIDTH, HEIGHT);

    for x in 0..100 as u32 {
        let mut img: DynamicImage = image::DynamicImage::new_rgb8(WIDTH,HEIGHT);
        let mut pixel: Rgba<u8> = Rgba([200; 4]);

        pixel[0] = (x*20 / 2) as u8;       // R
        pixel[1] = (255 - x*20 / 2) as u8;     // G
        pixel[2] = (x*20 % 255) as u8;      // B
        pixel[3] = 255;     // A

        // let xx: u32 = x % WIDTH;
        // let yy: u32 = x + 30 % WIDTH;
        for ii in 0..WIDTH {
            img.put_pixel(ii, 0, Rgba([50; 4]));
        }

        img.put_pixel(
            x%WIDTH,
            x%WIDTH,
            pixel,
        );

        // println!("------------------------------------------------------------");
        termpix::print_image(img, true_colour, width, height);
        let ten_millis = time::Duration::from_millis(50);
        thread::sleep(ten_millis);
    }
}
